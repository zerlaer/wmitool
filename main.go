package main

import (
	"bufio"
	"fmt"
	"github.com/StackExchange/wmi"
	"os"
	"strconv"
)

type System struct {
	Caption        string
	OSArchitecture string
}

type CPU struct {
	Name                      string
	NumberOfCores             uint32
	NumberOfLogicalProcessors uint32
}

type Memory struct {
	EndingAddress uint64
}

type Disk struct {
	Caption      string
	SerialNumber string
}
type Printer struct {
	Caption string
}

type Network struct {
	Description string
	MACAddress  string
	IPAddress   []string
}

func main() {

	{
		var system []System
		var disk []Disk
		var cpu []CPU
		var memory []Memory
		var network []Network
		var printer []Printer

		wmi.Query("Select * from Win32_OperatingSystem", &system)
		wmi.Query("Select * from Win32_Processor", &cpu)
		wmi.Query("Select * from Win32_MemoryArray", &memory)
		wmi.Query("Select * from Win32_DiskDrive", &disk)
		wmi.Query("Select * from Win32_Printer", &printer)
		wmi.Query("Select * from Win32_NetworkAdapterConfiguration where IPEnabled = True", &network)

		file, error := os.OpenFile("电脑信息.txt", os.O_RDWR|os.O_CREATE, 0666)
		if error != nil {
			fmt.Println(error)
			return
		}
		defer file.Close()
		writer := bufio.NewWriter(file)
		writer.WriteString("操作系统:" + system[0].Caption)
		writer.WriteString("\r\n系统架构:" + system[0].OSArchitecture)
		writer.WriteString("\r\n")
		for _, c := range cpu {
			writer.WriteString("\r\nCPU信息:" + c.Name)
			writer.WriteString("\r\nCPU核数:" + strconv.FormatUint(uint64(c.NumberOfCores), 10) + " 核")
			writer.WriteString("\r\nCPU线程数:" + strconv.FormatUint(uint64(c.NumberOfLogicalProcessors), 10) + " 线程")
		}
		writer.WriteString("\r\n")
		size := strconv.FormatUint((memory[0].EndingAddress / 1024 / 1000), 10)
		writer.WriteString("\r\n内存大小:" + size + "GB" + "\r\n")

		for _, n := range network {
			writer.WriteString("\r\n网卡名称:" + n.Description)
			writer.WriteString("\r\nIP地址:" + n.IPAddress[0])
			writer.WriteString("\r\nMAC地址:" + n.MACAddress)
		}
		writer.WriteString("\r\n")
		for _, d := range disk {
			writer.WriteString("\r\n硬盘名称:" + d.Caption)
			writer.WriteString("\r\n硬盘序号:" + d.SerialNumber)
		}
		writer.WriteString("\r\n")
		for _, p := range printer {
			writer.WriteString("\r\n打印机名称:" + p.Caption)

		}
		writer.Flush()
	}
}
