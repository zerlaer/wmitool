if ([System.Environment]::Is64BitOperatingSystem) {
    Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Export-Csv -Path "software.csv" -NoTypeInformation -Encoding UTF8
}
else {
    Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Export-Csv -Path "software.csv"  -NoTypeInformation  -Encoding UTF8
}
